"""This is module for testing CI/CD"""
import math


def get_hypotenuse(side_1, side_2):
    """Calc hypotenuse square triangle"""
    return math.sqrt(math.pow(side_1, 2) + (math.pow(side_2, 2)))


def get_area(side_1, side_2):
    """Calc area square triangle"""
    return side_1 * side_2 / 2


if __name__ == "__main__":
    a = int(input("Input a:"))
    b = int(input("Input b:"))
    print("c = ", get_hypotenuse(a, b))
    print("S = ", get_area(a, b))
